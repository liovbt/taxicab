#!/bin/bash

dims=$(seq 1000 1000 49000)
repeat=10000

for d in ${dims}; do
  ./gen.sh ${d} 100 > ${d}.txt
done

for impl in impl1 impl2 impl3; do
  for d in ${dims}; do
    t=$(taxicab -f ${d}.txt -i ${impl} -t -n ${repeat})
    echo ${d} $(echo "${t}/${repeat}" | bc -l) >> ${impl}.dat
  done
done
