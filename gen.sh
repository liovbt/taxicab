#!/bin/bash
len=$1
modulo=$2
chars=LR
for i in $(seq 1 $len); do
  echo -n "${chars:RANDOM%${#chars}:1}"
  echo $((RANDOM%modulo))
done
