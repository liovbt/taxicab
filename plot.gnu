#!/usr/bin/gnuplot
set xrange [0:*]
set yrange [0:*]
set xlabel 'Problem dimension'
set ylabel 'Execution time (ms)'
set key left top
plot 'impl1.dat' pt 6, 'impl2.dat' pt 6, 'impl3.dat' pt 6
pause -1
