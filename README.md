# taxicab

## Problem definition

You are facing north, you can either turn left (L) or right (R) 90 degrees and
walk forward the given number of blocks, ending at a new intersection.

given:
R1, R1, R3, R1, R1, L2, R5, L2, R5, R1, R4, L2, R3, L3, R4, L5, R4, R4, R1, L5, L4, R5, R3, L1, R4, R3, L2, L1, R3, L4, R3, L2, R5, R190, R3, R5, L5, L1, R54, L3, L4, L1, R4, R1, R3, L1, L1, R2, L2, R2, R5, L3, R4, R76, L3, R4, R191, R5, R5, L5, L40, L5, L3, R1, R3, R2, L2, L2, L4, L5, L4, R5, R4, R4, R2, R3, R4, L3, L2, R5, R3, L2, L1, R2, L3, R2, L1, L1, R1, L3, R5, L5, L1, L2, R5, R3, L3, R3, R5, R2, R5, R5, L5, L5, R25, L3, L5, L2, L1, R2, R2, L2, R2, L3, L2, R3, L5, R4, L4, L5, R3, L4, R1, R3, R2, R4, L2, L3, R2, L5, R5, R4, L2, R4, L1, L3, L1, L3, R1, R2, R1, L5, R5, R3, L3, L3, L2, R4, R2, L5, L1, L1, L5, L4, L1, L1, R1

What is the shortest path to the finish?

For example:
- R2, L3 leaves you 2 blocks east and 3 blocks north (shortest path would be 5)
- R2, R2, R2 leaves you 2 blocks south of start (shortest path would be 2)
- R5, L5, R5, R3 would give you a shortest path of 12

## Implementations

* impl1: trivial sequential loop
* impl2: recursive solution, no loop
* impl3: concurrent implementation with 4 goroutines each doing a sequential loop

## Usage

```
taxicab L1 L2 R1
taxicab -i impl2 -f input.txt
taxicab -i impl3 -f 2000.txt -n 10000 -t # Run the algorithm 10000 times and print total execution time instead of result
```

## Unit test

```
go test
```
