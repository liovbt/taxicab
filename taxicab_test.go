package main

import "testing"

func Test(t *testing.T) {
  cases := []struct {
    in []string
    want int
  }{
    {[]string{"R2", "L3"}, 5},
    {[]string{"R2", "R2", "R2"}, 2},
    {[]string{"R5", "L5", "R5", "R3"}, 12},
  }
  for name, impl := range TaxicabImplementations {
    for _, c := range cases {
      got, err := impl(c.in)
      if err != nil {
        t.Errorf("%v(%q) failed: %v", name, c.in, err)
        continue
      }
      if got != c.want {
        t.Errorf("%v(%q) == %v, want %v", name, c.in, got, c.want)
      }
    }
  }
}
