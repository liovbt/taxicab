package main

import "strconv"
import "os"
import "fmt"
import "errors"
import "flag"
import "io/ioutil"
import "regexp"
import "time"

type Vector struct { x, y int }
type Rotation struct { XX, XY, YX, YY int }

type State struct {
  pos, dir Vector
}

var NORTH = Vector { 0, 1 }
var LEFT  = Rotation { 0, -1, 1, 0 }
var RIGHT = Rotation { 0, 1, -1, 0 }

func (v *Vector) Rotate(rot Rotation) {
  x := v.x * rot.XX + v.y * rot.XY
  v.y = v.x * rot.YX + v.y * rot.YY
  v.x = x
}

func (v *Vector) Add(w Vector) {
  v.x += w.x
  v.y += w.y
}

func (v Vector) Mul(n int) Vector {
  return Vector { v.x * n, v.y * n }
}

func (state *State) Move(rot Rotation, dist int) {
  state.dir.Rotate(rot)
  state.pos.Add(state.dir.Mul(dist))
}

func (state State) ShortestPath() int {
  return Abs(state.pos.x) + Abs(state.pos.y)
}

func Abs(x int) int {
  if (x > 0) { return x }
  return -x
}

func ParseStep (step string) (Rotation, int, error) {
  var turn Rotation
  switch char := step[0]; char {
    case 'L': turn = LEFT
    case 'R': turn = RIGHT
    default: return Rotation{}, 0, errors.New("Does not start with 'L' or 'R'")
  }
  var i int
  for i = 1; i < len(step); i++ {
    if step[i] < '0' || step[i] > '9' {
      break
    }
  }
  dist, err := strconv.Atoi(step[1:i])
  return turn, dist, err
}

func Impl1 (path []string) (int, error) {
  state := State { dir: NORTH }
  for _, step := range path {
    turn, dist, err := ParseStep(step)
    if err != nil {
      return -1, errors.New("Failed to parse step \""+step+"\": "+err.Error())
    }
    state.Move(turn, dist)
  }
  return state.ShortestPath(), nil
}

func relativeRotation(u Vector, v Vector) Rotation {
  var r11 int = u.x*v.x+u.y*v.y
  var r12 int = u.y*v.x-u.x*v.y
  return Rotation { r11, r12, -r12, r11 }
}

func recurse (path []string) (*State, error) {
  var n int = len(path)/2
  if n == 0 {
    s := State{ dir: NORTH }
    if len(path) == 0 {
      return &s, nil
    }
    turn, dist, err := ParseStep(path[0])
    if err != nil {
      return nil, err
    }
    s.Move(turn, dist)
    return &s, nil
  } else {
    s1, err := recurse(path[:n])
    if err != nil {
      return nil, err
    }
    s2, err := recurse(path[n:])
    if err != nil {
      return nil, err
    }
    s2.pos.Rotate(relativeRotation(NORTH, s1.dir))
    s1.pos.Add(s2.pos)
    s1.dir.Rotate(relativeRotation(NORTH, s2.dir))
    return s1, nil
  }
}

func Impl2 (path []string) (int, error) {
  s, err := recurse(path)
  if err != nil {
    return -1, err
  }
  return s.ShortestPath(), nil
}

func work(path []string, res_chan chan State, err_chan chan error) {
  state := State { dir: NORTH }
  for _, step := range path {
    turn, dist, err := ParseStep(step)
    if err != nil {
      err_chan <- errors.New("Failed to parse step \""+step+"\": "+err.Error())
    }
    state.Move(turn, dist)
  }
  res_chan <- state
}

func Impl3 (path []string) (int, error) {
  workers := 4
  results := make([]chan State, workers)
  errors := make([]chan error, workers)
  for i := 0 ; i < workers ; i++ {
    results[i] = make(chan State)
    errors[i] = make(chan error)
    cut := len(path)/(workers-i)
    go work(path[:cut], results[i], errors[i])
    path = path[cut:]
  }

  state := State{ dir: NORTH }
  for i:=0 ; i < workers ; i++ {
    select {
    case s := <- results[i]:
      s.pos.Rotate(relativeRotation(NORTH, state.dir))
      state.pos.Add(s.pos)
      state.dir.Rotate(relativeRotation(NORTH, s.dir))
    case e := <- errors[i]:
      return -1, e
    }
  }
  return state.ShortestPath(), nil
}

type TaxicabImpl func([]string) (int, error);

var TaxicabImplementations = map[string] TaxicabImpl {
  "impl1": Impl1,
  "impl2": Impl2,
  "impl3": Impl3,
}

func main() {
  repeat := flag.Int("n", 1, "number of times to repeat the calculation")
  impl_name := flag.String("i", "impl1", "implementation to use")
  input_file := flag.String("f", "", "file to load input from (otherwise loaded from command line args)")
  measure_time := flag.Bool("t", false, "Measure execution time")
  flag.Parse()
  impl, ok := TaxicabImplementations[*impl_name]
  if !ok {
    fmt.Println("No implementation named", *impl_name)
    os.Exit(1)
  }

  var path []string
  if *input_file == "" {
    path = flag.Args()
  } else {
    content, err := ioutil.ReadFile(*input_file)
    if err != nil {
      fmt.Println(err)
      os.Exit(1)
    }
    stepRegexp := regexp.MustCompile("[LR]\\d+")
    path = stepRegexp.FindAllString(string(content), -1)
  }

  var result int
  var err error
  start := time.Now()
  for i := 0; i < *repeat ; i++ {
    result, err = impl(path)
    if err != nil {
      fmt.Println(err)
      os.Exit(1)
    }
  }
  if *measure_time {
    elapsed := time.Since(start).Milliseconds()
    fmt.Println(elapsed)
  } else {
    fmt.Println(result)
  }
}
